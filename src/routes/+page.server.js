import db from '$lib/db.js'

export async function load() {
  try {
    const createTable = db.prepare(
      `CREATE TABLE IF NOT EXISTS customer_review (
        id INTEGER PRIMARY KEY,
        customer_name TEXT,
        star_rating INTEGER,
        comment TEXT
      )`
    );
    createTable.run();
    const reviews = await db.prepare(`SELECT * FROM customer_review`).all();
    return { reviews };
  } catch (e) {
    console.error(e);
    throw error;
  }
}

export const actions = {
  review: async ({ request }) => {
    const data = await request.formData();
    const customer_name = data.get('customer_name');
    const star_rating = parseInt(data.get('star_rating'));
    const comment = data.get('comment');

    const createTable = db.prepare(
      `CREATE TABLE IF NOT EXISTS customer_review (
        id INTEGER PRIMARY KEY,
        customer_name TEXT,
        star_rating INTEGER,
        comment TEXT
      )`
    );
    createTable.run();

    const insert = db.prepare(`INSERT INTO customer_review (
      customer_name,
      star_rating,
      comment
    ) VALUES (?,?,?)`);
    insert.run(customer_name, star_rating, comment);

    return { success: true };
  },

  reservation: async ({ request }) => {
    const data = await request.formData();
    const customer_name = data.get('customer_name');
    const phone_number = data.get('phone_number');
    const service = data.get('service');
    const date = data.get('date');
    const time = data.get('time');

    const createTable = db.prepare(
      `CREATE TABLE IF NOT EXISTS customer_reservation (
        id INTEGER PRIMARY KEY,
        customer_name TEXT,
        phone_number TEXT,
        service TEXT,
        date TEXT,
        time TEXT
      )`
    );
    createTable.run();

    const insert = db.prepare(`INSERT INTO customer_reservation (
      customer_name,
      phone_number,
      service,
      date,
      time
    ) VALUES (?,?,?,?,?)`);
    insert.run(customer_name, phone_number, service, date, time);

    return { success: true };
  }
}
