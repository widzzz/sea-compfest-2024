FROM alpine:3.20
RUN apk add nodejs npm
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

ENV ORIGIN=http://localhost:3000
EXPOSE 3000
CMD ["node","build"]
