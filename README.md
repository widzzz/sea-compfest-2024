# widzzz/compfest-2024

This is a project for submission `Technical Challenge: SEA Salon` at COMPFEST 2024.

# Check out the deployment here [https://compfest.widzzz.com](https://compfest.widzzz.com)

## Tech stack used

- Svelte 4 with SvelteKit
- sqlite3
- TailwindCSS

## Requirements

- NodeJS v20 LTS

## How to run in dev environment

This is the fastest way to run this app. **Clone this repo first!**

Install dependencies
```sh
npm install
```

Run the development server
```sh
npm run dev
# Usually it will be at http://localhost:5173
```

**No need to initialize database!** App will generate `project.db` file for your database when you run it.

## How to run with Docker

I am not really recommending this way since it usually emits a cross-site error if not configured correctly.

Build the image. Still, **no need to initialize database!**

```sh
docker build -t compfest-test .
```

After the build process completes, run the built Docker image.

```sh
docker run -dp 3000:3000 --name compfest-test-container test-compfest
# Make sure you configure it to run at port 3000 otherwise it will emit cross-site error
```
