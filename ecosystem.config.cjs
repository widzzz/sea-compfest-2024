module.exports = {
    apps: [{
        name: 'sea-compfest-2024',
        script: './build/index.js',
        instances: 'max',
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'production',
            PORT: 80,
            ORIGIN: 'https://compfest.widzzz.com'
        }
    }]
};
